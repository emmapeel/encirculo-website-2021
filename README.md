New 2021 website for encirculo.org

## Add translations

Content translations happen on Weblate.

To enable a translation, add its ISO name into `_config.yml`.  The order
doesn't matter except for the first item in the array, which becomes the
default language.

```diff
 locales:
 - es # default language
 - en
+- pt_BR # add new language
```

Create a data file if Weblate hasn't already done so:

```bash
cp _data/locale.json _data/pt_BR.json
```

Some images like screenshots require translations.  If you don't have or
require them, you can create a symbolic link from another locale:

```bash
ln -sv es assets/images/pt_BR # Brazilian Portuguese speakers will see Spanish screenshots
```

**Important:** The `locale_name` key is always the language name, so for
Spanish it will be "Español" and for English it will be "English".

## Videos

Videos need to be 16:9 aspect ratio.

To add new videos, increase the `developers_videos` counter to match the
amount of videos on the `_data/LANG.json` file.

If the video is hosted at a video streaming platform, add
a `developer_video_X_url` and `developer_video_X_image` keys to the
language file, where `X` is the video number.  They can't be duplicated,
and the amount of X must match the `developers_videos` counter.

If the video is hosted locally, the adding process is the same, but
you'll also need to provide two versions, in 720p and 360p for
responsive video.

You can convert the video files with `ffmpeg`:

```bash
# assets/videos/video.mp4 is the video file to be converted

# 360p
ffmpeg -i assets/videos/video.mp4 \
       -vf scale=w=640:h=360 \
        -c:v libx264 -x264-params "nal-hrd=cbr:force-cfr=1" \
        -b:v 1M -maxrate:v 1M -minrate:v 1M -bufsize:v 1M \
        -preset slow -g 48 -sc_threshold 0 -keyint_min 48 \
        -c:a aac -b:a 48k -ac 2 \
        assets/videos/video_360p.mp4

# 720p
ffmpeg -i assets/videos/video.mp4 \
       -vf scale=w=1280:h=720 \
       -c:v libx264 -x264-params "nal-hrd=cbr:force-cfr=1" \
       -b:v 3M -maxrate:v 3M -minrate:v 3M -bufsize:v 3M \
       -preset slow -g 48 -sc_threshold 0 -keyint_min 48 \
       -c:a aac -b:a 96k -ac 2 \
        assets/videos/video_720p.mp4
```

It's important that the 720p and 360p files are placed in the same
directory and suffixed as such.  This is how the site produces the
responsive videos.

[Conversion guide
source](https://ottverse.com/hls-packaging-using-ffmpeg-live-vod/)
(though there's no HLS support on this site currently.)

### Subtitles

Add a third key called `developer_video_X_subtitles` pointing to the
[WebVTT](https://developer.mozilla.org/en-US/docs/Glossary/WebVTT) file
in the current locale.
